package com.game.tictactoe.calculator;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

@SpringBootTest
public class CalculatorImplTest {
    private CalculatorImpl calculator;

    @Test
    public void givenAAndBThenC() {
        //Arrange
        calculator = new CalculatorImpl();
        int a = 10;
        int b = 9;
        int c = 19;
        //Act
        int result = calculator.add(a, b);

        //Assert
        assertThat(result).isEqualTo(c);

    }

    @Test
    public void givenAAndBSubtractThenC(){
        //Arrange
        calculator = new CalculatorImpl();
        int a = 10;
        int b = 9;
        int c = 1;
        //Act
        int result = calculator.subtract(a, b);
        //Assert
        assertThat(result).isEqualTo(c);

    }

    @Test
    public void givenAAndBMultiplyThenC(){
        //Arrange
        calculator = new CalculatorImpl();
        int a = 10;
        int b = 9;
        int c = 90;
        //Act
        int result = calculator.multiply(a, b);
        //Assert
        assertThat(result).isEqualTo(c);

    }

    @Test
    public void givenAAndBDivideThenC(){
        //Arrange
        calculator = new CalculatorImpl();
        int a = 10;
        int b = 5;
        int c = 2;
        //Act
        int result = calculator.divide(a, b);
        //Assert
        assertThat(result).isEqualTo(c);

    }

    @Test
    public void givenAAndBWhenDivideByZeroThenException(){
        //Arrange
        calculator = new CalculatorImpl();
        int a = 10;
        int b = 0;
        //Act
       assertThatThrownBy(()->calculator.divide(a,b)).isInstanceOf(ArithmeticException.class).hasMessage("/ by zero");


    }
}
