package com.game.tictactoe;



import com.fasterxml.jackson.databind.ObjectMapper;

import com.game.controller.TicTacToeController;
import com.game.dto.Result;
import com.game.dto.StepsDTO;
import com.game.tictactoe.repo.TicTacToe;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(TicTacToeController.class)
public class TicTacToeControllerTest {

    @MockBean
    private TicTacToeController game;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void whenPlayThenNoWinner() throws Exception {

        StepsDTO moveDto = new StepsDTO(1, 1);
        when(game.play(moveDto)).thenReturn(Result.builder().result("No Winner").build());

        //Act
        mockMvc.perform(
                post("/api/game")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(moveDto))
        ).andExpect(status().isOk());
    }

    @Test
    public void whenGetAllBoardBeforePlay() throws Exception {
        Character[][] board = new Character[][]{
                {'\0', '\0', '\0'},
                {'\0', '\0', '\0'},
                {'\0', '\0', '\0'}
        };

        when(game.getBoard()).thenReturn(Result.builder().board(board).build());

        String json = objectMapper.writeValueAsString(Result.builder().board(board).build());

        mockMvc.perform(get("/api/board")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(json));
    }
    @Test
    public void whenXandOPlayGetBoardAfterFirstPlay() throws Exception {
        Character[][] board = new Character[][]{
                {'x', '\0', '\0'},
                {'\0', '\0', '\0'},
                {'\0', 'o', '\0'}
        };

        when(game.getBoard()).thenReturn(Result.builder().board(board).build());

        String json = objectMapper.writeValueAsString(Result.builder().board(board).build());

        mockMvc.perform(get("/api/board")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(json));
    }

}

