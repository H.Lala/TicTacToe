package com.game.tictactoe;


import com.game.tictactoe.domain.Steps;
import com.game.tictactoe.repo.StepsRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;


import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class TicTacToeTest {

    @Mock
    private StepsRepo stepsRepo;

    private TicTacToeImpl ticTacToe;

    @BeforeEach
    public final void before() {
        ticTacToe = new TicTacToeImpl(stepsRepo);
    }

    @Test
    public void whenXOutsideBoardThenRuntimeException() {

        assertThatThrownBy(() -> ticTacToe.play(5, 2)).
                isInstanceOf(RuntimeException.class)
                .hasMessage("Axis is outside of the board");

    }
    @Test
    public void whenYOutsideBoardThenRuntimeException() {

        assertThatThrownBy(() -> ticTacToe.play(2, 5))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Axis is outside of the board");

    }
    @Test
    public void whenOccupiedThenRuntimeException() {
        Steps step = new Steps();
        step.setXCord(0);
        step.setYCord(0);
        step.setPlayer('x');
        List stepsList = List.of(step);
        when(stepsRepo.findAll()).thenReturn(stepsList);

        assertThatThrownBy(()->ticTacToe.play(1, 1)) //y - player
                .isInstanceOf(RuntimeException.class).hasMessage("Piece is occupied");
    }

    @Test
    public void givenFirstTurnWhenNextPlayerThenX() {
        assertThat(ticTacToe.nextPlayer()).isEqualTo('x');
    }

    @Test
    public void givenLastTurnWasXWhenNextPlayerThenO() {
        Steps step = new Steps();
        step.setXCord(0);
        step.setYCord(0);
        step.setPlayer('x');
        List stepsList = List.of(step);
        when(stepsRepo.findAll()).thenReturn(stepsList);

        assertThat(ticTacToe.nextPlayer()).isEqualTo('o');
    }

    @Test
    public void whenPlayThenNoWinner() {
        String actual = ticTacToe.play(1,1);
        assertThat(actual).isEqualTo("No winner");
    }

    @Test
    public void whenPlayAndWholeHorizontalLineThenWinner() {
        Steps x1 = new Steps(1L,0,0,'x');
        Steps y1 = new Steps(2L,0,1,'o');
        Steps x2 = new Steps(3L,1,0,'x');
        Steps y2 = new Steps(4L,1,1,'o');

        when(stepsRepo.findAll()).thenReturn(List.of(x1,y1,x2,y2));
        assertThat(ticTacToe.play(3, 1)).isEqualTo("x is the winner");
    }

    @Test
    public void whenPlayAndWholeVerticalLineThenWinner() {
        Steps x1 = new Steps(1L,1,0,'x');
        Steps y1 = new Steps(2L,0,0,'o');
        Steps x2 = new Steps(3L,2,0,'x');
        Steps y2 = new Steps(4L,0,1,'o');
        Steps x3 = new Steps(5L,1,1,'x');

        when(stepsRepo.findAll()).thenReturn(List.of(x1,y1,x2,y2,x3));
        String actual = ticTacToe.play(1, 3); // O
        assertThat(actual).isEqualTo("o is the winner");
    }

    @Test
    public void whenPlayAndTopBottomDiagonalLineThenWinner() {
        Steps x1 = new Steps(1L,0,0,'x');
        Steps y1 = new Steps(2L,0,1,'o');
        Steps x2 = new Steps(3L,1,1,'x');
        Steps y2 = new Steps(4L,0,2,'o');
        when(stepsRepo.findAll()).thenReturn(List.of(x1,y1,x2,y2));
        assertThat(ticTacToe.play(3, 3)).isEqualTo("x is the winner");
    }

    @Test
    public void whenAllBoxesAreFilledThenDraw() {
        Steps x1 = new Steps(1L,0,0,'x');
        Steps y1 = new Steps(2L,0,1,'o');
        Steps x2 = new Steps(3L,0,2,'x');
        Steps y2 = new Steps(4L,1,0,'o');
        Steps x3 = new Steps(3L,1,2,'x');
        Steps y4 = new Steps(3L,1,1,'o');
        Steps x5 = new Steps(3L,2,0,'x');
        Steps y5 = new Steps(3L,2,2,'o');

        when(stepsRepo.findAll()).thenReturn(List.of(x1,y1,x2,y2,x3,y4,x5,y5));

        assertThat(ticTacToe.play(3, 2)).isEqualTo("The result is draw");
    }

}
