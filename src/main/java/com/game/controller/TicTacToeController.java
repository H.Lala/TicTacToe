package com.game.controller;


import com.game.dto.Result;
import com.game.dto.StepsDTO;
import com.game.tictactoe.TicTacToeImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class TicTacToeController {

    private final TicTacToeImpl ticTacToe;

    @PostMapping("/game")
    public Result play(@RequestBody StepsDTO stepsDTO) {
        return Result.builder().result(ticTacToe.play(stepsDTO.getX(),stepsDTO.getY())).build();
    }

   @GetMapping("/board")
    public Result getBoard(){
        return Result.builder().board(ticTacToe.getBoard()).build();
        }
}
