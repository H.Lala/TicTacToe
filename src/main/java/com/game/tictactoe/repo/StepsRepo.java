package com.game.tictactoe.repo;

import com.game.tictactoe.domain.Steps;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StepsRepo extends JpaRepository<Steps, Long> {
}
