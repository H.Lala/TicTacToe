package com.game.tictactoe.repo;

public interface TicTacToe {

 String play(int x, int y);
 Character[][] getBoard();
}
