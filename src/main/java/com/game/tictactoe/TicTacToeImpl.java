package com.game.tictactoe;

import com.game.tictactoe.domain.Steps;
import com.game.tictactoe.repo.StepsRepo;
import com.game.tictactoe.repo.TicTacToe;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TicTacToeImpl implements TicTacToe {

    private final StepsRepo stepsRepo;


    private static final int MaxSize = 3;

    @Override
    public String play(int x, int y) {
        checkAxis(x);
        checkAxis(y);
        Character[][] board = loadSteps();
        char lastPlayer = nextPlayer();
        setBox(x, y, lastPlayer, board);
        if (isWin(board,lastPlayer)) {
            return lastPlayer + " is the winner";
        } else if (isDraw(board)) {
            return "The result is draw";
        } else return "No winner";
    }

    @Override
    public Character[][] getBoard() {
        return loadSteps();
    }

    public char nextPlayer() {
        var steps = stepsRepo.findAll();
        if (steps.size() == 0) {
            return 'x';
        }
        var lastPlayer = steps.get(steps.size() - 1).getPlayer();
        return lastPlayer == 'x' ? 'o' : 'x';
    }
    public void saveSteps(int x, int y, char player) {
        Steps step = new Steps();
        step.setXCord(x);
        step.setYCord(y);
        step.setPlayer(player);

        stepsRepo.save(step);

    }
    private Character[][] loadSteps() {
        Character[][] board = {
                {'\0', '\0', '\0'},
                {'\0', '\0', '\0'},
                {'\0', '\0', '\0'}
        };
        stepsRepo.findAll().stream().
                forEach(steps -> board[steps.getXCord()][steps.getYCord()] = steps.getPlayer());
        printBoard(board);
        return board;
    }

    private void printBoard(Character[][] board) {
        for (int i = 0; i < board.length; i++) {
            System.out.println("");
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] == '\0') System.out.print("- ");
                System.out.print(board[i][j] + " ");
            }
        }


    }

    private boolean isDraw(Character[][] board) {
        for (int i = 0; i < MaxSize; i++) {
            for (int j = 0; j < MaxSize; j++) {
                if (board[i][j] == '\0')
                    return false;
            }
        }
        return true;
    }

    private boolean isWin(Character[][] board, char lastPlayer) {
        for (int i = 0; i < MaxSize; i++) {
            if (board[0][i] == lastPlayer && board[1][i] == lastPlayer && board[2][i] == lastPlayer) { //horizontal
                return true;
            } else if (board[i][0] == lastPlayer && board[i][1] == lastPlayer && board[i][2] == lastPlayer) { //vertical
                return true;
            }
        }

        if (board[0][0] == lastPlayer && board[1][1] == lastPlayer && board[2][2] == lastPlayer) {
            return true;
        }
        return false;
    }

    private void setBox(int x, int y, char lastPlayer, Character[][] board) {
        if (board[x - 1][y - 1] != '\0') {
            throw
                    new RuntimeException("Piece is occupied");
        } else {
            board[x - 1][y - 1] = lastPlayer;
            saveSteps(x, y, lastPlayer);
        }

    }

    private void checkAxis(int axis) {
        if (axis < 1 || axis > 3) {
            throw
                    new RuntimeException("Axis is outside of the board");
        }

    }




}
